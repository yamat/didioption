

class DefaultConfig(object):
    HOST = "localhost"
    DATABASE = "barchart"
    DB_USER = "yamat"
    DB_PASS = "default"
    DB_PORT = 5432
    SQLALCHEMY_DATABASE_URI = 'postgresql://' + DB_USER + ':' + DB_PASS + \
                              '@{0}:{1}/{2}'.format(HOST, DB_PORT, DATABASE)

    SQLALCHEMY_TRACK_MODIFICATIONS = False