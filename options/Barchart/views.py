from flask import jsonify
from options.app import app
from .trims import barchar
from .processing import get_top_pages
from .Models import Barchart
from options.admin.emails import send_options_data
import requests

@app.route("/")
def home():
    options = requests.get(barchar)
    new_row = Barchart()
    new_row.data = options.json()
    send_options_data(options.json())
    new_row.save()
    get_top_pages()
    return jsonify(new_row.data)

@app.route("/sendemail")
def send_email():
    pass